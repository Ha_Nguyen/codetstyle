[![pipeline status](https://gitlab.com/uilianries/native-floripa/badges/master/pipeline.svg)](https://gitlab.com/uilianries/native-floripa/commits/master)
[![coverage report](https://gitlab.com/uilianries/native-floripa/badges/master/coverage.svg)](https://gitlab.com/uilianries/native-floripa/commits/master)
[![Download](https://api.bintray.com/packages/uilianries/dpkg/hello/images/download.svg)](https://bintray.com/uilianries/dpkg/hello/_latestVersion)

# Native Floripa

## C++ Project example using Gitlab CI

#### Building
To build this Project

    $ mkdir build && cd build
    $ conan install ..
    $ cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF
    $ cmake --build .

#### Testing
To test this project

    $ mkdir build && cd build
    $ conan install ..
    $ cmake .. -DCMAKE_BUILD_TYPE=Debug
    $ cmake --build . --target test
    $ cmake --build . --target memcheck
    $ cmake --build . --target coverage

#### Packaging
To package this project

    $ mkdir build && cd build
    $ conan install ..
    $ cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF
    $ cmake --build .
    $ cpack -G DEB .

#### Documentation
To generate doxygen

    $ doxygen docs/Doxyfile
    $ firefox build/docs/html/index.html

#### Requirements
- CMake
- gcc or clang
- Conan
- Doxygen

#### LICENSE
[MIT](LICENSE)
