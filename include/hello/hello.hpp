#ifndef HELLO_HELLO_HPP
#define HELLO_HELLO_HPP

#include <string>

namespace hello {

    /*!
     * \brief Hello message class
     */
    class Message {
        public:

        /*!
         * \brief Get default message
         */
        std::string get_message() const;

        /*!
         * \brief Show default message on console
         */
        void show() const;
    };
}

#endif // HELLO_HELLO_HPP
